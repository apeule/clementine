package test;



import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.GregorianCalendar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import model.Senior;

@DisplayName("Test unitaire de la classe Senior")
class TestSenior {
	
	private Senior unSenior ;
	
	@BeforeEach
	void before() {
		GregorianCalendar dateNaiss = new GregorianCalendar(1940,9,30);
		unSenior = new Senior("1 40 05 44 523 45","Tesson",dateNaiss, "fzeffz@mail.com");
		
	}
	@Test
	@DisplayName("Test avec une date ant�rieure jour-mois")
	void testDonneAgeDateAnteAnniv() {
		
		LocalDate dateTest = LocalDate.of(2018,9,30);
		assertTrue(unSenior.donneAge(dateTest) == 78 , "Probl�me calcul age jour-mois ant�rieur"); 
	}
	
	@Test
	@DisplayName("Test avec une date post�rieure jour-mois")
	void testDonneAgeDatePostAnniv() {
		
		LocalDate dateTest = LocalDate.of(2018,9,30);
		assertTrue(unSenior.donneAge(dateTest) == 78 , "Probleme calcul age jour-mois post�rieur"); 
	}

}
