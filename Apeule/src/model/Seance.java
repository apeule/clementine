package model;

import java.util.GregorianCalendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="seance")
public class Seance {

	/**
	 * ID de la s�ance (primary key)
	 */
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "book_generator")
	@SequenceGenerator(name="book_generator", sequenceName = "idactiviteseq", allocationSize=1)
	@Column(name="code")
	private int code;
	
	/**
	 * La date de la s�ance
	 */
	
	@Column(name="dateseance")
	private GregorianCalendar dateseance;

	/**
	 * L'id de l'activit� (foreign key)
	 */
	
	@Column(name="idact")
	private int idact;

	
	/**
	 * CONSTRUCTEUR de la classe Seance
	 * @param code id de la s�ance cr�e
	 * @param dateSeance date de la s�ance cr�e
	 * @param idact id de l'activit�
	 */
	
	public Seance(GregorianCalendar dateSeance, int idact) {
		super();
		this.dateseance = dateSeance;
		this.idact = idact;
	}

	public Seance() {
		super();
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public GregorianCalendar getDateSeance() {
		return dateseance;
	}

	public void setDateSeance(GregorianCalendar dateSeance) {
		this.dateseance = dateSeance;
	}

	public int getIdact() {
		return idact;
	}

	public void setIdact(int idact) {
		this.idact = idact;
	}

	@Override
	public String toString() {
		return "Seance [code=" + code + ", dateseance=" + dateseance + ", idact=" + idact + "]";
	}
	
	
}
