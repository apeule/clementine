package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Classe d'objet m�tier ACTIVITE
 * @author ewen
 *
 */
@Entity
@Table(name="activite")
public class Activite {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "book_generator")
	@SequenceGenerator(name="book_generator", sequenceName = "idactiviteseq", allocationSize=1)
	@Column(name="identifiant")
	private int idActivite;
	
	@Column(name="designation")
	private String designation;

	

	/**
	 * @param idActivite
	 * @param designation
	 */
	public Activite(String designation) { 
		super();
		this.designation = designation;
	}
	
	public Activite() {
		super();
	}

	
	/**
	 * @return the idActivite
	 */
	public int getIdActivite() {
		return idActivite;
	}

	/**
	 * @param idActivite the idActivite to set
	 */
	public void setIdActivite(int idActivite) {
		this.idActivite = idActivite;
	}



	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}



	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}


	@Override
	public String toString() {
		return "Activite [idActivite=" + idActivite + ", designation=" + designation + "]";
	}

}
