package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;

import controller.Ctrl;
import model.Senior;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
 

/**
 * Classe de modification des informations d'un senior
 * @author ewen
 *
 */
public class ModifierSenior extends JFrame implements MyView {

	private static final long serialVersionUID = 1L;
	private JPanel contentPanel = new JPanel();
	public static JList<String> SeniorListe;
	
	private static JTextField txtNom;
	private static JTextField txtSecu;
	private static JTextField txtMail;
	
	private JButton btnModifier;
	private JButton btnFermer;
	
	private static JDateChooser dateChooserNaissance;
	private JButton btnAnnuler;

	/**
	 * M�thode permettant de mettre � jour le contenu de la liste des seniors
	 * @param liste Un objet ArrayList contenant des objets Senior � int�grer dans l'ihm
	 */
	public static void setSeniorListe(List<Senior> liste){
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(Senior s : liste){
			model.addElement(s.getNom());
		}
		SeniorListe.setModel(model);
	}
	
	
	/**
	 * M�thode statique permettant d'obtenir le contenu du champ texte nom
	 * @return le contenu du champ texte nom
	 */
	public static String getTxtNom(){
		return txtNom.getText();
	}
	/**
	 * M�thode statique permettant d'obtenir le contenu du champ texte secu
	 * @return le contenu du champ texte secu
	 */
	public static String getTxtSecu(){
		return txtSecu.getText();
	}
	/**
	 * M�thode statique permettant d'obtenir le contenu du champ texte mail
	 * @return le contenu du champ texte mail
	 */
	public static String getTxtMail(){
		return txtMail.getText();
	}
	
	/**
	 * M�thode statique permettant d'obtenir le contenu du champ calendrier de date de naissance
	 * @return le contenu du champ calendrier date naissance
	 */
	public static GregorianCalendar getdateChooserNaissance() {
		return (GregorianCalendar)dateChooserNaissance.getCalendar();
	}
	
	/**
	 * M�thode statique permettant de placer le curseur dans le champ texte de numero de secu
	 */
	public static void focusTxtSecu(){
		ModifierSenior.txtSecu.requestFocus();
	}
	
	/**
	 * M�thode statique permettant de r�initialiser les champs
	 */
	public static void init(){
		txtSecu.setText("");
		txtNom.setText("");
		txtMail.setText("");
		dateChooserNaissance.setCalendar(null);
	}
	/**
	 * Create the dialog.
	 * @param liste Un objet ArrayList contenant des objets Senior � int�grer dans l'ihm
	 */
	public ModifierSenior(List<Senior> liste) {
		setTitle("Senior - Modifier");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPanel = new JPanel();
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanel);
		contentPanel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(40, 36, 167, 84);
		contentPanel.add(scrollPane);
		
		SeniorListe = new JList<String>();
		scrollPane.setViewportView(SeniorListe);
		SeniorListe.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setSeniorListe(liste);
		
		
		btnFermer = new JButton("Fermer");
		btnFermer.setBounds(335, 227, 89, 23);
		contentPanel.add(btnFermer);
		btnFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		btnModifier = new JButton("Modifier");
		btnModifier.setBounds(143, 227, 89, 23);
		contentPanel.add(btnModifier);
		
		btnAnnuler = new JButton("Annuler");
		btnAnnuler.setBounds(242, 227, 89, 23);
		contentPanel.add(btnAnnuler);
		
		JLabel lblChoixS = new JLabel("Choix du s\u00E9nior");
		lblChoixS.setBounds(40, 11, 143, 14);
		contentPanel.add(lblChoixS);
		
		// LES INFORMATIONS
		txtNom = new JTextField();
		txtNom.setBounds(146, 175, 86, 20);
		contentPanel.add(txtNom);
		txtNom.setColumns(10);
		
		txtSecu = new JTextField();
		txtSecu.setBounds(146, 144, 86, 20);
		contentPanel.add(txtSecu);
		txtSecu.setColumns(10);
		
		txtMail = new JTextField();
		txtMail.setBounds(338, 144, 86, 20);
		contentPanel.add(txtMail);
		txtMail.setColumns(10);
		
		dateChooserNaissance = new JDateChooser();
		dateChooserNaissance.setBounds(337, 175, 87, 20);
		contentPanel.add(dateChooserNaissance);
		
		// LES LABELS 
		JLabel lblSecu = new JLabel("Num\u00E9ro de s\u00E9cu :");
		lblSecu.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSecu.setBounds(10, 146, 126, 14);
		contentPanel.add(lblSecu);
		
		JLabel lblNom = new JLabel("Nom : ");
		lblNom.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNom.setBounds(90, 178, 46, 14);
		contentPanel.add(lblNom);
		
		JLabel lblEmail = new JLabel("Email :");
		lblEmail.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEmail.setBounds(282, 147, 46, 14);
		contentPanel.add(lblEmail);
		
		JLabel lblDateDeNaissance = new JLabel("Date naissance :");
		lblDateDeNaissance.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDateDeNaissance.setBounds(242, 178, 89, 14);
		contentPanel.add(lblDateDeNaissance);
	
		
	}

	@Override
	public void assignListener(Ctrl ctrl) {
		this.btnModifier.setActionCommand("ModifierSenior_modifier");
		this.btnModifier.addActionListener(ctrl);
		
		this.btnAnnuler.setActionCommand("ModifierSenior_annuler");
		this.btnAnnuler.addActionListener(ctrl);
		
	}
}
