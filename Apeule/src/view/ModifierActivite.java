package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.Ctrl;
import model.Activite;


/**
 * Classe d�finissant la vue de modification d'une activit�
 * @author ewen
 *
 */
public class ModifierActivite extends JFrame implements MyView{

	private static final long serialVersionUID = 1L;
	
	public static JList<String> listeActivite;
	
	private JPanel contentPane;
	private static JTextField txtDesignation;
	
	private JButton btnModifier;
	private JButton btnFermer;
	private JButton btnAnnuler;
	
	/**
	 * M�thode permettant de mettre � jour le contenu de la liste des activites
	 * @param liste Un objet ArrayList contenant des objets Activite � int�grer dans l'ihm
	 */
	public static void setListeActivite(List<Activite> liste){
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(Activite act : liste){
			model.addElement(act.getDesignation());
		}
		listeActivite.setModel(model);
	}
	
	/**
	 * M�thode statique permettant d'obtenir le contenu du champ texte de la designation
	 * @return le contenu du champ texte de la designation
	 */
	public static String getTxtDesignation() {
		return txtDesignation.getText();
	}
	
	/**
	 * M�thode statique permettant de placer le curseur dans le champ texte de la designation
	 */
	public static void focusTxtDesignation(){
		ModifierActivite.txtDesignation.requestFocus();
	}
	
	/**
	 * M�thode statique permettant de r�initialiser les champs
	 */
	public static void init(){
		txtDesignation.setText("");
	}

	/**
	 * Create the dialog.
	 * @param liste Un objet ArrayList contenant des objets Activite � int�grer dans l'ihm
	 */
	public ModifierActivite(List<Activite> liste) {
		setTitle("Activit� - Modifier");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblChoixDeLactivit = new JLabel("Choix de l'activit\u00E9 :");
		lblChoixDeLactivit.setBounds(20, 11, 118, 22);
		contentPane.add(lblChoixDeLactivit);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(30, 44, 118, 96);
		contentPane.add(scrollPane);
		
		listeActivite = new JList<String>();
		scrollPane.setViewportView(listeActivite);
		setListeActivite(liste);
		
		JLabel lblDesignation = new JLabel("Designation :");
		lblDesignation.setBounds(168, 55, 97, 14);
		contentPane.add(lblDesignation);
		
		txtDesignation = new JTextField();
		txtDesignation.setBounds(296, 52, 86, 20);
		contentPane.add(txtDesignation);
		txtDesignation.setColumns(10);
		
		btnModifier = new JButton("Modifier");
		btnModifier.setBounds(119, 227, 103, 23);
		contentPane.add(btnModifier);
		
		btnFermer = new JButton("Fermer");
		btnFermer.setBounds(335, 227, 89, 23);
		contentPane.add(btnFermer);
		
		btnAnnuler = new JButton("Annuler");
		btnAnnuler.setBounds(236, 227, 89, 23);
		contentPane.add(btnAnnuler);

		btnFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	@Override
	public void assignListener(Ctrl ctrl) {
		this.btnModifier.setActionCommand("ModifierActivite_modifier");
		this.btnModifier.addActionListener(ctrl);
		
		this.btnAnnuler.setActionCommand("ModifierActivite_annuler");
		this.btnAnnuler.addActionListener(ctrl);
		
	}
}
