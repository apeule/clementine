package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import controller.Ctrl;
import model.Activite;
import model.Senior;
/**
 * Classe d�finissant la vue de choix d'un s�nior pour effectuer une inscription
 * @author xavier
 *
 */
public class ChoixSeniorList extends JFrame implements MyView{

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private static JList<String> listSeniors;
	private static JList<String> listActivites;
	
	/**
	 * M�thode permettant de mettre � jour le contenu de la liste des s�niors
	 * @param liste Un objet ArrayList contenant des objets Senior � int�grer dans l'ihm
	 */
	public static void setListSeniors(List<Senior> liste){
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(Senior s : liste){
			model.addElement(s.getNom());
		}
		listSeniors.setModel(model);
	}
	
	/**
	 * M�thode permettant de mettre � jour le contenu de la liste des activit�s
	 * @param liste Un objet ArrayList contenant des objets Activite � int�grer dans l'ihm
	 */
	public static void setListActivites(List<Activite> liste){
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(Activite act : liste){
			model.addElement(act.getDesignation());
		}
		listActivites.setModel(model);
	}
	
	
	

	/**
	 * Create the dialog.
	 * @param liste Un objet ArrayList contenant des objets Senior � int�grer dans l'ihm
	 * @param liste Un objet ArrayList contenant des objets Activite � int�grer dans l'ihm
	 */
	public ChoixSeniorList(List<Senior> listeS, List<Activite> listeA) {
		setTitle("S\u00E9nior - Inscrire");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(41, 25, 167, 84);
		contentPanel.add(scrollPane);
		
		listSeniors = new JList<String>();
		scrollPane.setViewportView(listSeniors);
		listSeniors.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setListSeniors(listeS);
		
		JLabel lblSenior = new JLabel("Etape 1 : choix du s\u00E9nior");
		lblSenior.setBounds(0, 0, 434, 14);
		contentPanel.add(lblSenior);
		
		JLabel lblActivite = new JLabel("Etape 2 : choix des activit\u00E9s");
		lblActivite.setBounds(0, 131, 434, 14);
		contentPanel.add(lblActivite);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(47, 156, 165, 61);
		contentPanel.add(scrollPane_1);
		
		listActivites = new JList<String>();
		scrollPane_1.setViewportView(listActivites);
		setListActivites(listeA);
	

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		JButton btnFermer = new JButton("Fermer");
		btnFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		JButton btnInscrire = new JButton("Inscrire");
		buttonPane.add(btnInscrire);
		buttonPane.add(btnFermer);

	}

	@Override
	public void assignListener(Ctrl ctrl) {
		
	}
}