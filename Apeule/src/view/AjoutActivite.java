package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.Ctrl;
import javax.swing.JLabel;

/**
 * Classe d�finissant la vue d'ajout d'une activit�
 * @author ewen
 *
 */
public class AjoutActivite extends JFrame implements MyView{


	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	
	private JButton btnValider;
	private JButton btnFermer;
	private static JTextField txtActivite;
	private JButton btnAnnuler;

	/**
	 * M�thode statique permettant de r�initialiser les champs
	 */
	public static void init(){
		txtActivite.setText("");
	}
	
	/**
	 * M�thode statique permettant d'obtenir le contenu du champ texte activite
	 * @return le contenu du champ texte activite
	 */
	public static String getTxtDesignation(){
		return txtActivite.getText();
	}

	/**
	 * Create the dialog.
	 */
	public AjoutActivite() {
		setTitle("Activit� - Ajouter");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblActivit = new JLabel("D\u00E9signation de l'activit\u00E9 :");
			lblActivit.setBounds(10, 86, 156, 14);
			contentPanel.add(lblActivit);
		}

		txtActivite = new JTextField();
		txtActivite.setBounds(176, 83, 86, 20);
		contentPanel.add(txtActivite);
		txtActivite.setColumns(10);

		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				btnValider = new JButton("Valider");
				buttonPane.add(btnValider);
				getRootPane().setDefaultButton(btnValider);
			}
			{
				btnFermer = new JButton("Fermer");
				btnFermer.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
			}
				{
					btnAnnuler = new JButton("Annuler");
					buttonPane.add(btnAnnuler);
				}
				buttonPane.add(btnFermer);
		}
	}


	@Override
	public void assignListener(Ctrl ctrl) {
		this.btnValider.setActionCommand("AjoutActivite_valider");
		this.btnValider.addActionListener(ctrl);

		this.btnAnnuler.setActionCommand("AjoutActivite_annuler");
		this.btnAnnuler.addActionListener(ctrl);
	}

}
