package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.toedter.calendar.JDateChooser;

import controller.Ctrl;
import model.Activite;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * Classe d�finissant la vue d'ajout d'une s�ance
 * @author ewen
 *
 */
public class AjoutSeance extends JDialog implements MyView{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JButton btnValider;
	private JButton btnFermer;
	private static JDateChooser dateSeance;
	private JList list;
	
	public static void setListeActivite(List<Activite> liste){
		DefaultListModel model = new DefaultListModel();
		for(Activite act : liste){
			model.addElement(act.getDesignation());
		}
	}
	
	
	/**
	 * M�thode statique permettant d'obtenir le contenu du champ calendrier de dateSeance
	 * @return le contenu du champ calendrier dateSeance
	 */
	public static GregorianCalendar getDateSeance() {
		return (GregorianCalendar)dateSeance.getCalendar();
	}
	
	public AjoutSeance(List<Activite> liste) {
		setTitle("S�ance - Ajouter");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		contentPanel.setLayout(null);

		
		
		btnValider = new JButton("Valider");
		btnValider.setBounds(199, 227, 89, 23);
		getContentPane().add(btnValider);
		
		btnFermer = new JButton("Fermer");
		btnFermer.setBounds(292, 227, 89, 23);
		getContentPane().add(btnFermer);
		

		btnFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		JDateChooser dateSeance = new JDateChooser();
		dateSeance.setBounds(258, 81, 87, 20);
		getContentPane().add(dateSeance);
		
		list = new JList();
		list.setBounds(37, 28, 130, 168);
		getContentPane().add(list);
		setListeActivite(liste);
		
		}

	@Override
	public void assignListener(Ctrl ctrl) {
		this.btnValider.setActionCommand("AjoutSeance_valider");
		this.btnValider.addActionListener(ctrl);
		
	}
}
