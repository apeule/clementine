package view;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.Ctrl;
import library.ActiviteDAO;
import library.SeanceDAO;
import library.SeniorDAO;

/**
 * Classe d�finissant la vue g�n�rale de l'application
 * @author xavier, ewen
 *
 */
public class MainView extends JFrame implements MyView{

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnSenior;
	private JButton btnInscription;
	private JButton btnAjoutSeance;
	private JButton btnModifier;
	private JButton btnActivite;
	private JButton btnSuppAct;
	private JButton btnSupSen;
	private JButton btnModifAct;

	/**
	 * Launch the application.
	 * @param args arguments du main
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//Cr�ation du contr�leur
					Ctrl ctrl = Ctrl.getCtrl();
					//Cr�ation de la vue g�n�rale
					MainView frame = new MainView();
					//Assignation d'un observateur sur cette vue
					frame.assignListener(ctrl);
					//Affichage de la vue
					frame.setVisible(true);
					SeniorDAO.init();
					SeanceDAO.init();
					ActiviteDAO.init();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainView() {
		setTitle("Cl�mentine - Accueil");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 452, 373);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnSenior = new JButton("Ajout d'un nouveau s�nior");
		btnSenior.setBounds(54, 11, 309, 23);
		contentPane.add(btnSenior);
		
		btnInscription = new JButton("Inscription d'un s�nior � des activit�s");
		btnInscription.setBounds(54, 79, 309, 23);
		contentPane.add(btnInscription);
		
		btnAjoutSeance = new JButton("Ajout d'une nouvelle s\u00E9ance");
		btnAjoutSeance.setBounds(54, 266, 309, 23);
		contentPane.add(btnAjoutSeance);
		
		btnModifier = new JButton("Modification d'un s\u00E9nior");
		btnModifier.setBounds(54, 45, 309, 23);
		contentPane.add(btnModifier);
		
		btnSupSen = new JButton("Supprimer un s\u00E9nior");
		btnSupSen.setBounds(54, 113, 309, 23);
		contentPane.add(btnSupSen);
		
		btnActivite = new JButton("Ajout d'une activit\u00E9");
		btnActivite.setBounds(54, 157, 309, 23);
		contentPane.add(btnActivite);
		
		btnSuppAct = new JButton("Supprimer une activit\u00E9");
		btnSuppAct.setBounds(54, 191, 309, 23);
		contentPane.add(btnSuppAct);
		
		btnModifAct = new JButton("Modifier une activit\u00E9");
		btnModifAct.setBounds(54, 225, 309, 23);
		contentPane.add(btnModifAct);
		

		JButton btnFermer = new JButton("Quitter");
		btnFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnFermer.setBounds(337, 300, 89, 23);
		contentPane.add(btnFermer);
			

	}

	@Override
	public void assignListener(Ctrl ctrl) {
		this.btnSenior.setActionCommand("MainView_senior");
		this.btnSenior.addActionListener(ctrl);
		
		this.btnInscription.setActionCommand("MainView_inscription");
		this.btnInscription.addActionListener(ctrl);
		
		this.btnAjoutSeance.setActionCommand("MainView_seance");
		this.btnAjoutSeance.addActionListener(ctrl);
		
		this.btnModifier.setActionCommand("MainView_modifier");
		this.btnModifier.addActionListener(ctrl);
		
		this.btnActivite.setActionCommand("MainView_activite");
		this.btnActivite.addActionListener(ctrl);
		
		this.btnSuppAct.setActionCommand("MainView_suppAct");
		this.btnSuppAct.addActionListener(ctrl);
		
		this.btnSupSen.setActionCommand("MainView_suppSen");
		this.btnSupSen.addActionListener(ctrl);
		
		this.btnModifAct.setActionCommand("MainView_modifAct");
		this.btnModifAct.addActionListener(ctrl);
	}
}
