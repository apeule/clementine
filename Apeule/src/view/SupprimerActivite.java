package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import controller.Ctrl;
import library.ActiviteDAO;
import model.Activite;
import javax.swing.JScrollPane;


/**
 * Classe d�finissant la vue de suppression d'une activit�
 * @author ewen
 *
 */
public class SupprimerActivite extends JFrame implements MyView{

	private static final long serialVersionUID = 1L;
	private JPanel contentPanel = new JPanel();
	private static JList<String> listeActivite;
	private JButton btnSupprimer;
	private JButton btnFermer;

	
	/**
	 * M�thode permettant de mettre � jour le contenu de la liste des activites
	 * @param liste Un objet ArrayList contenant des objets Activite � int�grer dans l'ihm
	 */
	public static void setListeActivite(List<Activite> liste){
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(Activite act : liste){
			model.addElement(act.getDesignation());
		}
		listeActivite.setModel(model);
	}
	
	
	/**
	 * Create the dialog.
	 * @param liste Un objet ArrayList contenant des objets Activite � int�grer dans l'ihm
	 */
	public SupprimerActivite(List<Activite> liste) {
		setTitle("Activit� - Supprimer");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPanel = new JPanel();
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanel);
		contentPanel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(46, 34, 191, 114);
		contentPanel.add(scrollPane);
		
		listeActivite = new JList<String>();
		scrollPane.setViewportView(listeActivite);
		listeActivite.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setListeActivite(liste);
		
		btnSupprimer = new JButton("Supprimer");
		btnSupprimer.setBounds(217, 227, 111, 23);
		contentPanel.add(btnSupprimer);
		
		btnFermer = new JButton("Fermer");
		btnFermer.setBounds(338, 227, 89, 23);
		contentPanel.add(btnFermer);
		btnFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
	}
	
	
	/**
	 * M�thode statique permettant de r�cup�rer l'identifiant de l'activit� s�lectionn�
	 * @return l'identifiant de l'activit� 
	 */
	public static int getIdAct() throws Exception{
		List<Activite> liste = null;
		liste = ActiviteDAO.getAll();
		int i = listeActivite.getSelectedIndex();
		Activite act = liste.get(i);
		int id = act.getIdActivite();
		return id;
	}
	
	public void assignListener(Ctrl ctrl) {

		this.btnSupprimer.setActionCommand("SupprimerActivite_supprimer");
		this.btnSupprimer.addActionListener(ctrl);
	}
}
