package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.Ctrl;
import library.SeniorDAO;
import model.Senior;
import javax.swing.JScrollPane;

/**
 * Classe d�finissant la vue de suppression d'un senior
 * @author ewen
 *
 */
public class SupprimerSenior extends JFrame implements MyView{

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static JList<String> listeSenior;
	private JButton btnSupprimer;
	private JButton btnFermer;
	private JScrollPane scrollPane;

	
	/**
	 * M�thode permettant de supprimer des activit�s
	 * @param liste Un objet ArrayList contenant des objets Activite � int�grer dans l'ihm
	 */
	public static void setListeSenior(List<Senior> liste){
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(Senior sen : liste){
			model.addElement(sen.getNom());
		}
		listeSenior.setModel(model);
	}
	
	/**
	 * Create the dialog.
	 * @param liste Un objet ArrayList contenant des objets Senior � int�grer dans l'ihm
	 */
	public SupprimerSenior(List<Senior> liste) {
		setTitle("Senior - Supprimer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnSupprimer = new JButton("Supprimer");
		btnSupprimer.setBounds(218, 227, 111, 23);
		contentPane.add(btnSupprimer);
		
		btnFermer = new JButton("Fermer");
		btnFermer.setBounds(339, 227, 89, 23);
		contentPane.add(btnFermer);
		btnFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(49, 40, 129, 147);
		contentPane.add(scrollPane);
		
		listeSenior = new JList<String>();
		scrollPane.setViewportView(listeSenior);
		setListeSenior(liste);
	}
	
	/**
	 * M�thode statique permettant de r�cup�rer le num�ro de secu du senior s�lectionn�
	 * @return le num�ro de s�cu du senior
	 */
	public static String getNumSecuSen() throws Exception{
		List<Senior> liste = null;
		liste = SeniorDAO.getAll();
		int i = listeSenior.getSelectedIndex();
		Senior sen = liste.get(i);
		String numSec = sen.getNumSecu();
		return numSec;
	}
	

	@Override
	public void assignListener(Ctrl ctrl) {
		this.btnSupprimer.setActionCommand("SupprimerSenior_supprimer");
		this.btnSupprimer.addActionListener(ctrl);
		
	}
}
