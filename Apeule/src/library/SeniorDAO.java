package library;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import model.Senior;
import view.ModifierSenior;
/**
 * Classe de persistance des objets dans une base SQL
 * @author xavier
 *
 */
public abstract class SeniorDAO {
	
	public static SessionFactory factory;
	public static Session session;
	
	public static void init()
	{
		factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(model.Senior.class)
				.buildSessionFactory();
	}
	
	public static void end()
	{
		factory.close();
	}
	
	/**
	 * M�thode de getAll des s�niors
	 * @return un objet List contenant tous les objets Senior
	 * @throws Exception e l'exception lev�e par Hibernate
	 */	
	public static List<Senior> getAll() throws Exception{
		
		session = factory.getCurrentSession();
		
		Transaction transaction = session.beginTransaction();
		
		List<Senior> seniors;
		
		try{
			
			seniors=session.createQuery("from Senior").getResultList();
			transaction.commit();
		}
		catch(Exception e) 
		{
			transaction.rollback();
			throw e;
		}

		return seniors;
	}

	
	/**
	 * M�thode d'INSERT d'un nouveau s�nior
	 * @param le senior � ins�rer
	 * @return le senior
	 * @throws Exception l'exception lev�e par Hibernate
	 */
	public static Senior insert(Senior s) throws Exception{

		session = factory.getCurrentSession();

		Transaction transaction = session.beginTransaction();

		try{

			session.save(s);
			transaction.commit();
		}
		catch(Exception e) 
		{
			transaction.rollback();
			throw e;
		}
		
		return s;
	}
	
	/**
	 * M�thode DELETE d'un SENIOR
	 * @param num�ro de secu
	 * @return num�ro de s�cu
	 * @throws Exception l'exception lev�e par Hibernate
	 */
	public static String delete(String numSecu) throws Exception{
		
		session = factory.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		
		try {
			Senior sen = session.get(Senior.class, numSecu);
			session.delete(sen);
			transaction.commit();
			
		}
		catch(Exception e) {
			session.getTransaction().rollback();
			throw e;
		}
		
		return numSecu;
		
	}
	
	/**
	 * M�thode UPDATE d'un SENIOR
	 * @param nom du senior
	 * @return null
	 * @throws Exception l'exception lev�e par Hibernate
	 */
	public static Senior update(String nom) throws Exception{
		
        session = factory.getCurrentSession();
		Transaction transaction = session.beginTransaction();     
		
		try{
			String nomS = ModifierSenior.getTxtNom();
			String mail = ModifierSenior.getTxtMail();                    

		
		List<Senior> seniors = session.createQuery("from Senior s where s.nom like '" + nom + "'").getResultList();
		
		for (Senior senior : seniors) {
			senior.setNom(nomS); 
			senior.setMail(mail); 
		}       
		   
		transaction.commit();     
		} 
		catch(Exception e) { 
			e.printStackTrace();
			transaction.rollback(); 
			throw e; 
		} 
		return null;

	}
	
}
