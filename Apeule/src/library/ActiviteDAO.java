package library;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import model.Activite;
import model.Senior;
import view.ModifierActivite;
import view.ModifierSenior;
/**
 * Classe de persistance des objets dans une base SQL
 * @author ewen
 *
 */
public abstract class ActiviteDAO {
	
	public static SessionFactory factory;
	public static Session session;
	
	public static void init()
	{
		factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(model.Activite.class)
				.buildSessionFactory();
	}
	
	public static void end()
	{
		factory.close();
	}
	
	/**
	 * M�thode de getAll des activit�s
	 * @return un objet List contenant tous les objets Activite
	 * @throws Exception e l'exception lev�e par Hibernate
	 */	
	public static List<Activite> getAll() throws Exception{
		
		session = factory.getCurrentSession();
		
		Transaction transaction = session.beginTransaction();
		
		List<Activite> activites;
		
		try{
			
			activites=session.createQuery("from Activite").getResultList();
			transaction.commit();
		}
		catch(Exception e) 
		{
			transaction.rollback();
			throw e;
		}

		return activites;
	}

	
	/**
	 * M�thode d'INSERT d'une nouvelle activite
	 * @param l'activite � ins�rer
	 * @return l'activite
	 * @throws Exception l'exception lev�e par Hibernate
	 */
	public static Activite insert(Activite act) throws Exception{

		session = factory.getCurrentSession();

		Transaction transaction = session.beginTransaction();

		try{

			session.save(act);
			transaction.commit();
		}
		catch(Exception e) 
		{
			transaction.rollback();
			throw e;
		}
		
		return act;
	}
	

	/**
	 * M�thode DELETE d'une activit�
	 * @param id de l'activit�
	 * @return id de l'activit�
	 * @throws Exception l'exception lev�e par Hibernate
	 */
	
	public static int delete(int idact) throws Exception{
		
		session = factory.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		
		try {
			Activite act = session.get(Activite.class, idact);
			session.delete(act);
			transaction.commit();
			
		}
		catch(Exception e) {
			session.getTransaction().rollback();
			throw e;
		}
		
		return idact;
		
	}
	
	
	/**
	 * M�thode UPDATE d'une activit�
	 * @param designation de l'activit�
	 * @return null
	 * @throws Exception l'exception lev�e par Hibernate
	 */
	public static Activite update(String designation) throws Exception{
		
        session = factory.getCurrentSession();
		Transaction transaction = session.beginTransaction();     
		
		try{
			String designationAct = ModifierActivite.getTxtDesignation();
		
		List<Activite> activites = session.createQuery("from Activite act where act.designation like '" + designation + "'").getResultList();
		
		for (Activite act : activites) {
			act.setDesignation(designationAct); 
		}       
		   
		transaction.commit();     
		} 
		catch(Exception e) { 
			e.printStackTrace();
			transaction.rollback(); 
			throw e; 
		} 
		return null;

	}
	

}
