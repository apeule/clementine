package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.JOptionPane;

import library.ActiviteDAO;
import library.SeniorDAO;
//import library.SeanceDAO;
import model.Activite;
import model.Senior;
import view.AjoutActivite;
import view.AjoutSeance;
import view.AjoutSenior;
import view.ChoixSeniorList;
import view.ModifierActivite;
import view.ModifierSenior;
import view.SupprimerActivite;
import view.SupprimerSenior;
/**
 * Classe CONTROLEUR
 * @author xavier
 *
 */
public class Ctrl implements ActionListener{

	/**
	 * CONSTRUCTEUR priv� de la classe Ctrl
	 */
	private Ctrl(){
	}
	/**
	 * Instance unique pr�-initialis�e
	 */
	private static Ctrl CTRL = new Ctrl();
	/**
	 * Point d'acc�s pour l'instance unique du singleton
	 * @return le singleton Ctrl
	 */
	public static Ctrl getCtrl(){
		return CTRL;
	}

	/**
	 * M�thode d�clanch�e lors de clics sur les boutons de l'application
	 */
	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent evt) {
		//R�cup�ration de l'action
		String action = evt.getActionCommand();
		//D�coupage et analyse de celle-ci
		String details[] = action.split("_");
		//who : QUI ? Quelle vue a effectu� l'action ?
		String who = details[0];
		//what : QUOI ? Qu'est-ce-que cette vue souhaite faire ?
		String what = details[1];
		//switch-case de traitement des diff�rents cas
		switch(who){
		case "MainView":
			switch(what){
			
			/**
			 * Case de la vue AjoutActivite
			 */
			case "activite":
				AjoutActivite frameAct = new AjoutActivite();
				frameAct.assignListener(this);
				frameAct.setVisible(true);
				break;
			/**
			 * Case de la vue AjoutSenior
			 */
			case "senior":
				AjoutSenior frame = new AjoutSenior();
				frame.assignListener(this);
				frame.setVisible(true);
				break;
			/**
			 * Case de la vue ChoixSeniorList
			 */
			case "inscription":
				//Cr�ation de la vue de s�lection du s�nior pour lequel on souhaite effectuer des inscriptions
				List<Senior> listeS=null;
				List<Activite> listeA=null;
				try {
					listeS = SeniorDAO.getAll();
					listeA = ActiviteDAO.getAll();
				} catch (Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				ChoixSeniorList frameInscription = new ChoixSeniorList(listeS, listeA);
				//Assignation d'un observateur sur cette vue
				frameInscription.assignListener(this);
				//Affichage de la vue
				frameInscription.setVisible(true);
				break;
				
			/**
			 * Case de la vue AjoutSeance
			 */
			case "seance":
				List<Activite> listeDesActivitesSeances = null;
				try {
					listeDesActivitesSeances = ActiviteDAO.getAll();
				} catch (Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				//Cr�ation de la vue d'ajout de s�ance
				AjoutSeance frameAjoutSeance = new AjoutSeance(listeDesActivitesSeances);
				//Assignation d'un observateur sur cette vue
				frameAjoutSeance.assignListener(this);
				//Affichage de la vue
				frameAjoutSeance.setVisible(true);
				break;
				
			/**
			 * Case de la vue ModifierSenior
			 */
			case "modifier":
				//Cr�ation de la vue de modification d'un s�nior
				List<Senior> liste1 = null;
				try {
					liste1 = SeniorDAO.getAll();
				} catch (Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				ModifierSenior frameModifierSenior = new ModifierSenior(liste1);
				//Assignation d'un observateur sur cette vue
				frameModifierSenior.assignListener(this);
				//Affichage de la vue
				frameModifierSenior.setVisible(true);
				break;
				
			/**
			 * Case de la vue SupprimerActivite
			 */
			case "suppAct":
				List<Activite> listeDesActivites = null;
				try {
					listeDesActivites = ActiviteDAO.getAll();
				} catch (Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				SupprimerActivite frameSuppAct = new SupprimerActivite(listeDesActivites);
				frameSuppAct.assignListener(this); 
				frameSuppAct.setVisible(true);
				break;
				
			/**
			 * Case de la vue SupprimerSenior
			 */
			case "suppSen":
				List<Senior> listeDesSeniors = null;
				try {
					listeDesSeniors = SeniorDAO.getAll();
				} catch (Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				SupprimerSenior frameSuppSen = new SupprimerSenior(listeDesSeniors);
				frameSuppSen.assignListener(this); 
				frameSuppSen.setVisible(true);
				break;
				
				
			/**
			 * Case de la vue ModifierActivite
			 */
			case "modifAct":
				//Cr�ation de la vue de modification d'une activit�
				List<Activite> liste2 = null;
				try {
					liste2 = ActiviteDAO.getAll();
				} catch (Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				ModifierActivite frameModifierActivite = new ModifierActivite(liste2);
				//Assignation d'un observateur sur cette vue
				frameModifierActivite.assignListener(this);
				//Affichage de la vue
				frameModifierActivite.setVisible(true);
				break;
				
				
			}
			break;
			
				
		case "AjoutSenior":
			switch(what){
			case "valider":
				//R�cup�ration des informations saisies par l'utilisateur
				String nom = AjoutSenior.getTxtName();
				if(nom.equals("")){
					JOptionPane.showMessageDialog(null,"Le nom du s�nior � �t� omis. Veillez � le saisir correctement.","Erreur Saisie",JOptionPane.WARNING_MESSAGE);
					AjoutSenior.focusTxtName();
				}
				else{
					String nomS = AjoutSenior.getTxtName();
					String numS = AjoutSenior.getTxtNumSecu();
					GregorianCalendar dateN = AjoutSenior.getdateChooserNaissance();
					String email = AjoutSenior.getTxtMail();
					
					//Cr�ation du nouvel objet Senior
					Senior senior = new Senior(numS,nomS,dateN, email);
					//INSERT dans la BD
					try {
						SeniorDAO.insert(senior);
						//Message de confirmation pour l'utilisateur
						JOptionPane.showMessageDialog(null,"Le s�nior a bien �t� ajout�","Confirmation Enregistrement",JOptionPane.INFORMATION_MESSAGE);
						//R�initialisation des champs
						AjoutSenior.init();
					} catch (Exception e) {
						String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
						JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
					}
				}
				break;
				
			case "annuler":
				//R�initialisation des champs
				AjoutSenior.init();
				break;
				
			}
			break;
			
		case "AjoutActivite":
			switch(what){
			case "valider":
				//R�cup�ration des informations saisies par l'utilisateur
				String designation = AjoutActivite.getTxtDesignation();
				if(designation.equals("")){
					JOptionPane.showMessageDialog(null,"Le nom de l'activit� n'est pas valide.","Erreur Saisie",JOptionPane.WARNING_MESSAGE);
				}
				else{
					String designationAct = AjoutActivite.getTxtDesignation();
					System.out.println(designationAct);
					//Cr�ation du nouvel objet Activite
					Activite act = new Activite(designationAct);
					//INSERT dans la BD
					try {
						ActiviteDAO.insert(act);
						//Message de confirmation pour l'utilisateur
						JOptionPane.showMessageDialog(null,"L'activit� a bien �t� ajout�","Confirmation Enregistrement",JOptionPane.INFORMATION_MESSAGE);
						//R�initialisation des champs
						AjoutActivite.init();
					} catch (Exception e) {
						String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
						JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
					}
				}
				break;
				
			case "annuler":
				//R�initialisation des champs
				AjoutActivite.init();
				break;
			}
			break;
			
		case "SupprimerActivite":
			switch(what) {
			case "supprimer":
				int idact;
				try {
					idact = SupprimerActivite.getIdAct();
					ActiviteDAO.delete(idact);
					JOptionPane.showMessageDialog(null,"L'activit� a bien �t� supprim�e","Confirmation Enregistrement",JOptionPane.INFORMATION_MESSAGE);
					SupprimerActivite.setListeActivite(ActiviteDAO.getAll());
				}
				catch(Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				break;
			}
			break;
			
		case "SupprimerSenior":
			switch(what) {
			case "supprimer":
				String idsen;
				try {
					idsen = SupprimerSenior.getNumSecuSen();
					SeniorDAO.delete(idsen);
					JOptionPane.showMessageDialog(null,"Le senior a bien �t� supprim�e","Confirmation Enregistrement",JOptionPane.INFORMATION_MESSAGE);
					SupprimerSenior.setListeSenior(SeniorDAO.getAll());
				}
				catch(Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				break;
			}
			break;
			
			
		case "ModifierSenior":
			switch(what) {
			case "modifier":
				
				String sen = ModifierSenior.SeniorListe.getSelectedValue();
				
				try {
					SeniorDAO.update(sen);
					JOptionPane.showMessageDialog(null,"Le senior a bien �t� modifi�","Confirmation Enregistrement",JOptionPane.INFORMATION_MESSAGE);
				}
				catch(Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				break;
				
			case "annuler":
				//R�initialisation des champs
				ModifierSenior.init();
				break;
				
			}
			
		case "ModifierActivite":
			switch(what) {
			case "modifier":
				String act = ModifierActivite.listeActivite.getSelectedValue();
				
				try {
					ActiviteDAO.update(act);
					JOptionPane.showMessageDialog(null,"L'activit� a bien �t� modifi�","Confirmation Enregistrement",JOptionPane.INFORMATION_MESSAGE);
				}
				catch(Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				break;
				
			case "annuler":
				//R�initialisation des champs
				ModifierActivite.init();
				break;
				
			}
			
			
			break;
			
			
		case "AjoutSeance":
			switch(what) {
			case "valider":
				//R�cup�ration des informations saisies par l'utilisateur
				GregorianCalendar dateS = AjoutSeance.getDateSeance();

				//int idActivite = AjoutSeance.listeActivite.getSelectedRow();
//				Integer.valueOf(idActivite);
//				System.out.println(idActivite);
				//Cr�ation du nouvel objet Seance
				//Seance seance = new Seance(dateS, idActivite);
				//INSERT dans la BD
				try {
					//SeanceDAO.insert(seance);
					//Message de confirmation pour l'utilisateur
					JOptionPane.showMessageDialog(null,"La s�ance a bien �t� ajout�","Confirmation Enregistrement",JOptionPane.INFORMATION_MESSAGE);
					//R�initialisation des champs

				} catch (Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				
				break;

		}
			break;
		
	}
}
}